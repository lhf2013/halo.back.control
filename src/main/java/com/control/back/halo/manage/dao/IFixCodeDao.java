package com.control.back.halo.manage.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.control.back.halo.basic.dao.IBaseDao;
import com.control.back.halo.manage.entity.FixCode;

@Repository
public interface IFixCodeDao extends IBaseDao<FixCode, Long> {

    List<FixCode> findByCodeType(Integer codeType);

    FixCode findByCode(Integer code);

    FixCode findByCodeTypeAndCode(Integer codeType, Integer code);

}
