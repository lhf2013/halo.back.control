function showFixcodeView(id) {
	var form = $("#manageFixcodeForm");
	form[0].reset();
	if (id == undefined) {
		$("#addFixcode").modal('show');
		return;
	}
	console.log(id)

	$("#idx").val(id);

	$.get("/fixcode/queryById/" + id, function(result) {
		form.find("input[name='codeName']").val(result.codeName);
		form.find("input[name='code']").val(result.code);
		form.find("input[name='codeType']").val(result.codeType);
	});

	$("#addFixcode").modal('show');
}

$(document).ready(function() {
	$('.fixcode-table').dataTable();
	// 更新
	$('.fixcode-update').click(function() {
		var id = $(this).attr("data-id");
		showFixcodeView(id);
	});
	// 添加用户
	$("#addFixcodeButton").click(function() {
		showFixcodeView();
	});
	// 删除
	$('.fixcode-remove').click(function() {
		var url = $(this).attr('xhref');
		swal({
			title : "您确定要删除这条信息吗",
			text : "删除后将无法恢复，请谨慎操作！",
			type : "warning",
			showCancelButton : true,
			confirmButtonColor : "#DD6B55",
			confirmButtonText : "删除",
			cancelButtonText : "取消",
			closeOnConfirm : false,
			closeOnCancel : false
		}, function(isConfirm) {
			if (isConfirm) {
				window.location.href = url;
			} else {
				swal("删除取消", "您取消了删除操作！", "error");
			}
		});
	});
	$.validator.setDefaults({
		highlight : function(e) {
			$(e).closest(".form-group").removeClass("has-success").addClass(
					"has-error")
		},
		success : function(e) {
			e.closest(".form-group").removeClass("has-error").addClass(
					"has-success")
		},
		errorElement : "span",
		errorPlacement : function(e, r) {
			e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent()
					.parent().parent() : r.parent())
		},
		errorClass : "help-block m-b-none",
		validClass : "help-block m-b-none"
	})
	$("#manageFixcodeForm").validate({
		rules : {
			"codeName" : {
				required : true
			},
			"code" : {
				required : true,
				number : true
			},
			"codeType" : {
				required : true,
				number : true
			}
		},
		messages : {
			"codeName" : {
				required : "请输入编码名称"
			},
			"code" : {
				required : "请输入编码",
				number : "必须输入数字"
			},
			"codeType" : {
				required : "请输入编码类型",
				number : "必须输入数字"
			}
		}
	});
});